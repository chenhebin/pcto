var rest = {
	get: function( url ){
		return this.internalCall("GET",url);
	},
	
	post: function ( url, payload ){
		return this.internalCall("POST",url,payload);
	},
	
	internalCall: function( method, url, payload ) {
		
		return new Promise(function(resolve,reject){
			var xhttp = new XMLHttpRequest();
			
			xhttp.onreadystatechange = function(){
				if(xhttp.readyState == 4){
					if(this.status >= 200 && this.status < 300){
						resolve(this.responseText);	
					}
					else{
						reject(this.status);
					}
				}
			};
			xhttp.open( "POST" , url, true);
			xhttp.send(payload);
		})
	},
}