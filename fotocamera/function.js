function getMedia() {
let constraints = {
video: {width: 600, height: 600},
};
let video = document.getElementById("video");
let promise = navigator.mediaDevices.getUserMedia(constraints);
promise.then(function (MediaStream) {
video.srcObject = MediaStream;
video.play();
});
}
function takePhoto() {
let video = document.getElementById("video");
let canvas = document.getElementById("canvas");
let ctx = canvas.getContext('2d');
ctx.drawImage(video, 0, 0, 500, 500);
}